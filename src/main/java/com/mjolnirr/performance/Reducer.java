package com.mjolnirr.performance;

import com.mjolnirr.lib.annotations.MjolnirrComponent;
import com.mjolnirr.lib.annotations.MjolnirrMethod;
import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by sk_ on 3/15/14.
 */
@MjolnirrComponent(componentName = "reducer")
public class Reducer extends AbstractModule {

    private ComponentContext context;

    @MjolnirrMethod
    public Map<String, Integer> reduce(File part) throws FileNotFoundException, InterruptedException {
        Map<String, Integer> result = new HashMap<String, Integer>();

        Scanner sc = new Scanner(new FileInputStream(part));
        while (sc.hasNext()) {
            String currentWord = sc.next().replaceAll("[^a-zA-Z ]", "").toLowerCase();

            if (result.containsKey(currentWord)) {
                result.put(currentWord, result.get(currentWord) + 1);
            } else {
                result.put(currentWord, 1);
            }
        }

        sc.close();
        part.delete();

        return result;
    }

    @Override
    public void initialize(ComponentContext componentContext) {
        this.context = componentContext;
    }
}
