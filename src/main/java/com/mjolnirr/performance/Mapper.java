package com.mjolnirr.performance;

import com.mjolnirr.lib.Callback;
import com.mjolnirr.lib.annotations.MjolnirrComponent;
import com.mjolnirr.lib.annotations.MjolnirrMethod;
import com.mjolnirr.lib.component.AbstractApplication;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.msg.HornetCommunicator;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by sk_ on 3/10/14.
 */
@MjolnirrComponent(componentName = "mapreducer")
public class Mapper extends AbstractApplication {
    private static final int PARTS_COUNT = 100;
    private ComponentContext context;

    @MjolnirrMethod
    public Map<String, Integer> analyze(File content) throws Exception {

        long startDistributionTime = System.currentTimeMillis();

        BufferedInputStream bis = new BufferedInputStream(
                new FileInputStream(content));
        FileOutputStream out;
        int partNumber = 0;
        int sizeOfFiles = (int) ((content.length() / PARTS_COUNT) + 1);
        byte[] buffer = new byte[sizeOfFiles];
        int tmp = 0;
        while ((tmp = bis.read(buffer)) > 0) {
            File newFile = new File("part" + partNumber++ + ".txt");
            newFile.createNewFile();
            out = new FileOutputStream(newFile);
            out.write(buffer, 0, tmp);
            out.close();
        }

        long endDistributionTime = System.currentTimeMillis();
        long distributionTime = endDistributionTime - startDistributionTime;

        /***********************************/

        final Map<String, Double> result = new ConcurrentHashMap<String, Double>();
        HornetCommunicator communicator = new HornetCommunicator();

        final Object sync = new Object();

        final int[] resultsCame = {0};
        for (int i = 0; i < partNumber; i++) {
            final File part = new File("part" + i + ".txt");

            final int finalPartNumber = partNumber;
            communicator.send(context, "reducer", "reduce", new ArrayList<Object>() {{
                        this.add(part);
                    }}, new Callback<Map<String, Double>>() {
                        @Override
                        public void run(Map<String, Double> reduceResult) {
                            synchronized (resultsCame) {
                                resultsCame[0]++;
                            }

                            for (String key : reduceResult.keySet()) {
                                if (result.containsKey(key)) {
                                    result.put(key, result.get(key) + reduceResult.get(key));
                                } else {
                                    result.put(key, reduceResult.get(key));
                                }
                            }

                            part.delete();

                            synchronized (resultsCame) {
                                if (resultsCame[0] == finalPartNumber) {
                                    synchronized (sync) {
                                        sync.notifyAll();
                                    }
                                }
                            }
                        }
                    }
            );
        }

        synchronized (sync) {
            sync.wait();
        }

        return sort(result);
    }

    private static Map sort(Map unsortedMap) {
        List list = new LinkedList(unsortedMap.entrySet());

        // sort list based on comparator
        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue())
                        .compareTo(((Map.Entry) (o1)).getValue());
            }
        });

        // put sorted list into map again
        //LinkedHashMap make sure order in which keys were inserted
        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

    public static void main(String args[]) throws Exception {
        File content = new File(args[0]);

        BufferedInputStream bis = new BufferedInputStream(
                new FileInputStream(content));
        FileOutputStream out;
        int partNumber = 0;
        int sizeOfFiles = (int) ((content.length() / PARTS_COUNT) + 1);
        byte[] buffer = new byte[sizeOfFiles];
        int tmp = 0;
        while ((tmp = bis.read(buffer)) > 0) {
            File newFile = new File("part" + partNumber++ + ".txt");
            newFile.createNewFile();
            out = new FileOutputStream(newFile);
            out.write(buffer, 0, tmp);
            out.close();
        }

        System.out.println(partNumber);
    }

    @Override
    public void initialize(ComponentContext componentContext) {
        this.context = componentContext;
    }
}
